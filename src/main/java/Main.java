import com.github.base.ServiceException;
import com.github.service.BarService;
import com.github.service.FooService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-context.xml");

        FooService fooService = context.getBean(FooService.class);
        BarService barService = context.getBean(BarService.class);

        try {
            barService.doFooServiceMethod();
            System.out.println(fooService.getByName("linxy"));
        } catch (ServiceException e) {
            e.printStackTrace();
        }


    }
}
