package com.github.dao;

import com.github.entity.Foo;
import org.springframework.stereotype.Repository;

@Repository
public class FooDao extends BaseDao<Foo, Integer> {

    public Foo getByName(String name) {
        Foo foo = new Foo();
        foo.setName(name);
        return foo;
    }
}
