package com.github.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;


public abstract class BaseDao<T, K extends Serializable> {

//    @Autowired
//    private SessionFactory sessionFactory;

//    public Session getCurrentSession() { return sessionFactory.getCurrentSession(); }

    private Class<T> clzEntity;

    @SuppressWarnings("unchecked")
    public BaseDao() {
        ParameterizedType type = (ParameterizedType)this.getClass().getGenericSuperclass();
        clzEntity = (Class<T>) type.getActualTypeArguments()[0];
        System.out.println(clzEntity.getSimpleName());
    }

    public K save(T entity) {
        System.out.println("save: " + entity);
        return null;
    }

    public void deleteById(K id) {
        System.out.println("deleteById: " + id);
    }

    public T findById(K id) {
        System.out.println("findById: " + id);
        return null;
    }

    public void update(T entity) {
        System.out.println("update: " + entity);
    }

}
