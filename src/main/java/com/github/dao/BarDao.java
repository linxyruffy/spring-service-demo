package com.github.dao;

import com.github.entity.Bar;
import org.springframework.stereotype.Repository;

@Repository
public class BarDao extends BaseDao<Bar, Integer>  {

    public Bar getByAddress(String address) {
        Bar bar = new Bar();
        bar.setAddress(address);
        return bar;
    }
}
