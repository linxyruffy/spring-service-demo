package com.github.base;

/**
 * Created by linxy on 2017/4/27.
 */
public class ServiceException extends Exception {

    private Integer code;

    public ServiceException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public ServiceException(ExceptionCode code) {
        super(code.getMessage());
        this.code = code.getCode();
    }

    public Integer getCode() {
        return this.code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
