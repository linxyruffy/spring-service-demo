package com.github.base;

import java.io.Serializable;

/**
 * Created by linxy on 2017/4/26.
 */
public interface IService<T, K extends Serializable> {

    K save(T entity) throws ServiceException;

    void deleteById(K id) throws ServiceException;

    T findById(K id) throws ServiceException;

    void update(T entity) throws ServiceException;
}