package com.github.base;

/**
 * Created by linxy on 2017/4/27.
 */
public enum ExceptionCode {
    EXCEPTION_PARAM(100, "Error Parameter");

    private Integer code;
    private String message;

    ExceptionCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
