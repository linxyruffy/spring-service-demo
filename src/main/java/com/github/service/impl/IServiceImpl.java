package com.github.service.impl;

import com.github.base.ExceptionCode;
import com.github.base.IService;
import com.github.base.ServiceException;
import com.github.dao.BaseDao;

import java.io.Serializable;

public class IServiceImpl<T, K extends Serializable, D extends BaseDao<T, K>> implements IService<T, K> {

    public D dao;

    public IServiceImpl(D dao) {
        this.dao = dao;
    }

    public K save(T entity) throws ServiceException {
        if(entity == null) {
            throw new ServiceException(ExceptionCode.EXCEPTION_PARAM);
        }
        return dao.save(entity);
    }

    public void deleteById(K id) throws ServiceException {
        if(id == null) {
            throw new ServiceException(ExceptionCode.EXCEPTION_PARAM);
        }
        dao.deleteById(id);
    }

    public T findById(K id) throws ServiceException {
        if(id == null) {
            throw new ServiceException(ExceptionCode.EXCEPTION_PARAM);
        }
        return dao.findById(id);
    }

    public void update(T entity) throws ServiceException {
        if(entity == null) {
            throw new ServiceException(ExceptionCode.EXCEPTION_PARAM);
        }
        dao.update(entity);
    }
}
