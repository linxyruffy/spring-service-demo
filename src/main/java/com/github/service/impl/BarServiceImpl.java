package com.github.service.impl;

import com.github.base.ExceptionCode;
import com.github.base.ServiceException;
import com.github.dao.BarDao;
import com.github.entity.Bar;
import com.github.entity.Foo;
import com.github.service.BarService;
import com.github.service.FooService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service(value = "barService")
public class BarServiceImpl  extends IServiceImpl<Bar, Integer, BarDao> implements BarService {

    @Resource
    private FooService fooService;

    @Autowired
    public BarServiceImpl(BarDao barDao) {
        super(barDao);
    }

    public Bar getByAddress(String address) throws ServiceException {
        if(address == null) {
            throw new ServiceException(ExceptionCode.EXCEPTION_PARAM);
        }
        return dao.getByAddress(address);
    }

    public void doFooServiceMethod() throws ServiceException {
        Foo foo = new Foo();
        fooService.save(foo);
        fooService.update(foo);
        fooService.findById(10);
        fooService.deleteById(10);
    }
}
