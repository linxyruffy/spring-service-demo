package com.github.service.impl;


import com.github.base.ExceptionCode;
import com.github.base.ServiceException;
import com.github.dao.FooDao;
import com.github.entity.Foo;
import com.github.service.FooService;
import org.springframework.stereotype.Service;


@Service(value = "fooService")
public class FooServiceImpl extends IServiceImpl<Foo, Integer, FooDao> implements FooService{

    public FooServiceImpl(FooDao dao) {
        super(dao);
    }

    public Foo getByName(String name) throws ServiceException {
        if(name == null) {
            throw new ServiceException(ExceptionCode.EXCEPTION_PARAM);
        }
        return dao.getByName(name);
    }
}
