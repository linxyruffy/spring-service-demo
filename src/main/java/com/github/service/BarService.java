package com.github.service;

import com.github.base.IService;
import com.github.base.ServiceException;
import com.github.entity.Bar;

/**
 * Created by linxy on 2017/4/27.
 */
public interface BarService extends IService<Bar, Integer>{

    Bar getByAddress(String address) throws ServiceException;

    void doFooServiceMethod() throws ServiceException;
}
