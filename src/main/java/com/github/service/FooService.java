package com.github.service;

import com.github.base.ServiceException;
import com.github.entity.Foo;
import com.github.base.IService;


public interface FooService extends IService<Foo, Integer> {
    Foo getByName(String name) throws ServiceException;
}
