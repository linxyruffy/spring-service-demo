package com.github.entity;


public class Bar extends BaseEntity<Integer> {

    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Bar{" +
                "address='" + address + '\'' +
                "} " + super.toString();
    }
}
