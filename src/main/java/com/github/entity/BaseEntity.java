package com.github.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by linxy on 2017/4/27.
 */
public abstract class BaseEntity<K extends Serializable> {
    private K id;
    private Date createTime;
    private Date updateTime;

    public K getId() {
        return id;
    }

    public void setId(K id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
